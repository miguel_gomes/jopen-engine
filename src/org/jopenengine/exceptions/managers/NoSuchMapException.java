/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.exceptions.managers;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>No Such Map Exception</b></p>
 * <p>Exception used to indicate that a given map could not be loaded.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 31 de Mar de 2011 23:41:58
 *
 */
public class NoSuchMapException extends Exception {
	private static final long serialVersionUID = -1917697898109618852L;
	
	/**
	 * Generic no such map exception.
	 */
	public NoSuchMapException() {
		super("Could not load map");
	}
	
	/**
	 * Generic no such map exception where the map name is given.
	 * @param mapName Map name
	 */
	public NoSuchMapException(String mapName) {
		super("Could not load supplied map \""+mapName+"\" from the local store.");
	}
	
	/**
	 * Exception thrown when it was not possible to load a given map and the cause is known.
	 * @param mapName Map name
	 * @param cause Exception that caused the load failure
	 */
	public NoSuchMapException(String mapName, Exception cause) {
		super("Could not load supplied map \""+mapName+"\" from the local store.\nCause: "+cause.getLocalizedMessage());
		super.initCause(cause);
	}
}
