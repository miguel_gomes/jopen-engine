package org.jopenengine.containers;

/**
 * Terrain features.
 * These stack on top of terrain types, though attributes may or may not stack.
 * @author fahrenheit
 *
 */
public enum TerrainFeature {
	/** Hills feature */
	HILLS ("Hills","Landform that extends above the surrounding terrain",2,25,0),
	/** Forest feature */
	FOREST ("Forest","Land that is covered with trees and shrubs",2,25,0),
	/** Jungle feature */
	JUNGLE ("Jungle","Dense, more or less impenetrable regions within a tropical rainforest with an abundance of animal and plant life",2,25,0),
	/** Mountain feature */
	MOUNTAIN ("Mountain","Land mass that projects well above its surroundings",4,25,0),
	/** River feature */
	RIVER ("River","Large natural stream of water",-1,0,-20),
	/** Marsh feature */
	MARSH ("Marsh","Low-lying wet land with grassy vegetation",2,0,0),
	/** Coast feature */
	COAST ("Coast","The shore of a sea or ocean",1,0,0),
	/** Oasis feature */
	OASIS ("Oasis","Fertile tract in a desert",0,0,0,true),
	/** Ice feature */
	ICE ("Ice","Frozen part of a body of water",0,0,0,true),
	/** Road feature */
	ROAD ("Road","Open way (generally public) for travel or transportation",0,0,0);
	
	// DEFINITION
	
	/** Feature identifier */
	private String identifier;
	/** Feature description */
	private String description;
	
	/** Feature movement cost (will not stack, Max) */
	private short movementCost;
	/** Feature base defence modifier percentage (as a short, divide by 100) */
	private short defenceModifier;
	/** Feature base attack modifier percentage (as a short, divide by 100) */
	private short attackModifier;
	/** Indicates that the Terrain that has this Feature can not be traversed, as in is interdicted for ALL units */
	private boolean interdicted;

	// CONSTRUCTORS AND METHODS
	
	/**
	 * Creates a new Terrain Feature enumeration.
	 * @param identifier Terrain identifier
	 * @param movementCost Terrain base movement cost
	 */
	TerrainFeature(String identifier, int movementCost) {
		this(identifier,"",movementCost);
	}
	
	/**
	 * Creates a new Terrain Feature enumeration.
	 * @param identifier Terrain identifier
	 * @param description Terrain description
	 * @param movementCost Terrain base movement cost
	 */
	TerrainFeature(String identifier, String description, int movementCost) {
		this(identifier, description, movementCost, 0, 0, false);
	}
	
	/**
	 * Creates a new Terrain Feature enumeration.
	 * @param identifier Terrain identifier
	 * @param movementCost Terrain base movement cost
	 * @param defenceModifier Terrain base defence modifier percentage (as a short, divide by 100)
	 */
	TerrainFeature(String identifier, int movementCost, int defenceModifier) {
		this(identifier, "", movementCost, defenceModifier);
	}
	
	/**
	 * Creates a new Terrain Feature enumeration.
	 * @param identifier Terrain identifier
	 * @param description Terrain description
	 * @param movementCost Terrain base movement cost
	 * @param defenceModifier Terrain base defence modifier percentage (as a short, divide by 100)
	 */
	TerrainFeature(String identifier, String description, int movementCost, int defenceModifier) {
		this(identifier, description, movementCost, defenceModifier, 0, false);
	}
	
	/**
	 * Creates a new Terrain Feature enumeration.
	 * @param identifier Terrain identifier
	 * @param movementCost Terrain base movement cost
	 * @param defenceModifier Terrain base defence modifier percentage (as a short, divide by 100)
	 * @param attackModifier Terrain base attack modifier percentage (as a short, divide by 100)
	 */
	TerrainFeature(String identifier, int movementCost, int defenceModifier, int attackModifier) {
		this(identifier, "", movementCost, defenceModifier, attackModifier);
	}
	
	/**
	 * Creates a new Terrain Feature enumeration.
	 * @param identifier Terrain identifier
	 * @param description Terrain description
	 * @param movementCost Terrain base movement cost
	 * @param defenceModifier Terrain base defence modifier percentage (as a short, divide by 100)
	 * @param attackModifier Terrain base attack modifier percentage (as a short, divide by 100)
	 */
	TerrainFeature(String identifier, String description, int movementCost, int defenceModifier, int attackModifier) {
		this(identifier, description, movementCost, defenceModifier, attackModifier, false);
	}
	
	/**
	 * Creates a new Terrain Feature enumeration.
	 * @param identifier Terrain identifier
	 * @param description Terrain description
	 * @param movementCost Terrain base movement cost
	 * @param defenceModifier Terrain base defence modifier percentage (as a short, divide by 100)
	 * @param attackModifier Terrain base attack modifier percentage (as a short, divide by 100)
	 * @param interdicted Indicates that the terrain can not be traversed, as in is interdicted for ALL units
	 */
	TerrainFeature(String identifier, String description, int movementCost, int defenceModifier, int attackModifier, boolean interdicted) {
		this.identifier = identifier;
		this.description = description;
		this.movementCost = (short) movementCost;
		this.defenceModifier = (short) defenceModifier;
		this.attackModifier = (short) attackModifier;
		this.interdicted = interdicted;
	}
	
	/**
	 * Gets the Terrain Feature identifier.
	 * @return Feature identifier
	 */
	public String getIdentifier() { return identifier; }
	
	/**
	 * Gets the Terrain Feature description.
	 * @return Feature description
	 */
	public String getDescription() { return description; }
	
	/**
	 * Gets the Terrain Feature base movement cost.
	 * @return Base movement cost in abstract units (will not stack for features)
	 */
	public short getMovementCost() { return movementCost; }
	
	/**
	 * Gets the Terrain Feature defence modifier.
	 * @return Defence modifier (from -100 to +100) (will not stack for features)
	 */
	public short getDefenceModifier() { return defenceModifier; }
	
	/**
	 * Gets the Terrain Feature attack modifier.
	 * @return Attack modifier (from -100 to +100) (will not stack for features)
	 */
	public short getAttackModifier() { return attackModifier; }
	
	/**
	 * Indicates if the terrain can be traversed or not.
	 * @return True if the terrain is interdicted to all units, false otherwise
	 */
	public boolean getIsInterditcted() { return interdicted; }
	
	public String toString() { return this.identifier; }
}
