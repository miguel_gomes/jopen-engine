/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.containers;

import java.util.ArrayList;

public class Tile implements TerrainTile {
	/** Terrain height in abstract units */ 
	private short height;
	/** Terrain type (can not change) */
	private final TerrainType type;
	/** Terrain features */
	private ArrayList<TerrainFeature> features;
	/** Movement cost of the tile (all things considered) */
	private int movementCost;
	
	/**
	 * Creates a new generic tile.
	 * @param terrainType The tile base terrain type
	 */
	public Tile(TerrainType terrainType) {
		movementCost = terrainType.getMovementCost();
		this.height = 0;
		this.type = terrainType;
		this.features = new ArrayList<TerrainFeature>();
	}
	
	/**
	 * Adds a new terrain feature to the tile.
	 * <p>Note: this is only possible if the base tile allows it and the feature is not already present.</p>
	 * @param feature Feature to add to the tile
	 * @return true if the feature was added successfully, false otherwise
	 */
	public boolean addTerrainFeature(TerrainFeature feature) {
		// TODO add tile validation
		if (features.contains(feature)) return false;
		features.add(feature);
		if (feature.getMovementCost() > movementCost) 
			movementCost = feature.getMovementCost();
		return true;
	}
	
	/**
	 * Remove a given terrain feature from the tile
	 * @param feature
	 */
	public void removeTerrainFeature(TerrainFeature feature) {
		features.remove(feature);
		// reset stuff
		resetMovementCost();
	}
	
	/**
	 * Resets the movement cost of the tile.
	 */
	private void resetMovementCost() {
		movementCost = type.getMovementCost();
		for (TerrainFeature feature: features)
			if (feature.getMovementCost() > movementCost) 
				movementCost = feature.getMovementCost();
	}
	
	/**
	 * Gets a list of terrain features.
	 * @return The list of terrain features
	 */
	public ArrayList<TerrainFeature> getFeatures() { return features; }
	
	/**
	 * Gets the terrain type.
	 * @return Terrain type
	 */
	public TerrainType getTerrainType() { return type; }
	
	/**
	 * Gets the tile height.
	 * @return Tile height in abstract units
	 */
	public int getHeight() { return height; }
	
	/**
	 * Sets the tile height.
	 * <p>Note: this overrides default tile heights.</p>
	 * @param height New height
	 */
	public void setTileHeight(int height) { this.height = (short) height; }
	
	/**
	 * Gets the movement cost of the tile (all things considered).
	 * @return Movement cost of the tile
	 */
	public int getMovementCost() { return movementCost; }
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(type.toString()).append(" tile (h=").append(height).append(")");
		if (!features.isEmpty()) {
			buf.append(" with: ");
			int i = 0;
			for (TerrainFeature feature: features) {
				buf.append(feature);
				if (i < features.size() -1) buf.append(", ");
				i++;
			}
		}
		return buf.toString(); 
	}
}
