package org.jopenengine.containers;

public class TerrainMap {
	/** Terrain rows (for fast access) */
	private int rows;
	/** Terrain columns (for fast access) */
	private int columns;
	/** Complete terrain map matrix */
	private TerrainTile[][] mapMatrix;
	
	/**
	 * Creates a new terrain map.
	 * @param rows Number of rows in the map
	 * @param columns Number of columns in the map
	 */
	public TerrainMap(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		mapMatrix = new Tile[columns][rows];
	}
	
	/**
	 * Adds a new terrain tile at a given location.
	 * @param row Tile row
	 * @param column Tile column
	 * @param tile Tile to add
	 */
	public void addTerrainTile(int row, int column, TerrainTile tile) {
		if (row < 0 || row >= rows || column < 0 || column >= columns) return;
		mapMatrix[column][row] = tile;
	}

	/**
	 * Gets a terrain tile at a given location.
	 * @param row Tile row
	 * @param column Tile column
	 * @return Terrain tile or null if not present
	 */
	public TerrainTile getTerrainTile(int row, int column) {
		if (row < 0 || row >= rows || column < 0 || column >= columns) return null;
		return mapMatrix[column][row];
	}
	
	/**
	 * Gets the number of rows of the current map.
	 * @return Number of rows
	 */
	public int getRowCount() { return rows; }
	
	/**
	 * Gets the number of columns of the current map.
	 * @return Number of columns
	 */
	public int getColumnCount() { return columns; }
}
