package org.jopenengine.containers;

/**
 * Basic Terrain types.
 * @author fahrenheit
 *
 */
public enum TerrainType {
	/** Grasslands base terrain type */
	GRASSLAND ("Grassland","Land where grass or grasslike vegetation grows and is the dominant form of plant life",1,-33),
	/** Plains base terrain type */
	PLAINS ("Plains","Land with relatively low relief, that is flat or gently rolling",1,-33),
	/** Desert base terrain type */
	DESERT ("Desert","Landscape or region that receives little precipitation",1,-33),
	/** Ocean base terrain type */
	OCEAN ("Ocean","Large body of water constituting a principal part of the hydrosphere",1),
	/** Lake base terrain type (inland mass of water) */
	LAKE ("Lake","Body of (usually fresh) water surrounded by land",1),
	/** Thundra base terrain type */
	THUNDRA ("Thundra","Vast treeless plain in the Arctic regions where the subsoil is permanently frozen",1),
	/** Snow base terrain type (permanent fixture) */
	GLACIER ("Glacier","Slowly moving mass of ice",1,-33);
	
	// DEFINITION
	
	/** Terrain identifier */
	private String identifier;
	/** Terrain description */
	private String description;
	
	/** Terrain base movement cost */
	private short movementCost;
	/** Terrain base defence modifier percentage (as a short, divide by 100) */
	private short defenceModifier;
	/** Indicates that the terrain can not be traversed, as in is interdicted for ALL units */
	private boolean interdicted;
	
	// TRANSFORMATION
	
	/** Terrain type to which the current terrain degrades over time */
	private TerrainType degradesToTerrain;
	/** Number of turns necessary for the terrain to degrade */
	private int degradesToTurns;
	/** Terrain type to which the current terrain transforms over time */
	private TerrainType transformsToTerrain;
	/** Number of turns necessary for the terrain to transform */
	private int transformsToTurns;
	
	// TILE PRODUCTION (NOT USED)
	
	
	
	// CONSTRUCTORS AND METHODS

	/**
	 * Creates a new default Terrain Type enumeration
	 */
	private TerrainType() {
		interdicted = false;
		movementCost = 1;
		defenceModifier = 0;
		description = "";
		degradesToTerrain = null;
		degradesToTurns = 0;
		transformsToTerrain = null;
		transformsToTurns = 0;
	}
	
	/**
	 * Creates a new Terrain Type enumeration.
	 * @param identifier Terrain identifier
	 * @param movementCost Base movement cost
	 */
	TerrainType(String identifier, int movementCost) {
		this(identifier, movementCost,0,false);
	}
	
	/**
	 * Creates a new Terrain Type enumeration.
	 * @param identifier Terrain identifier
	 * @param description Terrain description
	 * @param movementCost Base movement cost
	 */
	TerrainType(String identifier, String description, int movementCost) {
		this(identifier, description, movementCost,0,false);
	}
	
	/**
	 * Creates a new Terrain Type enumeration.
	 * @param identifier Terrain identifier
	 * @param movementCost Terrain base movement cost
	 * @param defenceModifier Terrain base defence modifier percentage (as a short, divide by 100)
	 */
	TerrainType(String identifier, int movementCost, int defenceModifier) {
		this(identifier, movementCost,defenceModifier, false);
	}
	
	/**
	 * Creates a new Terrain Type enumeration.
	 * @param identifier Terrain identifier
	 * @param description Terrain description
	 * @param movementCost Terrain base movement cost
	 * @param defenceModifier Terrain base defence modifier percentage (as a short, divide by 100)
	 */
	TerrainType(String identifier, String description, int movementCost, int defenceModifier) {
		this(identifier,description,movementCost,defenceModifier, false);
	}
	
	/**
	 * Creates a new Terrain Type enumeration.
	 * @param identifier Terrain identifier
	 * @param movementCost Terrain base movement cost
	 * @param defenceModifier Terrain base defence modifier percentage (as a short, divide by 100)
	 * @param interdicted Indicates that the terrain can not be traversed, as in is interdicted for ALL units
	 */
	TerrainType(String identifier, int movementCost, int defenceModifier, boolean interdicted) {
		this();
		this.identifier = identifier;
		this.movementCost = (short) movementCost;
		this.defenceModifier = (short) defenceModifier;
		this.interdicted = interdicted;
	}
	
	/**
	 * Creates a new Terrain Type enumeration.
	 * @param identifier Terrain identifier
	 * @param description Terrain description
	 * @param movementCost Terrain base movement cost
	 * @param defenceModifier Terrain base defence modifier percentage (as a short, divide by 100)
	 * @param interdicted Indicates that the terrain can not be traversed, as in is interdicted for ALL units
	 */
	TerrainType(String identifier, String description, int movementCost, int defenceModifier, boolean interdicted) {
		this(identifier,movementCost,defenceModifier, false);
		this.description = description;
	}
	
	/**
	 * Gets the Terrain Type identifier.
	 * @return Terrain identifier
	 */
	public String getIdentifier() { return identifier; }
	
	/**
	 * Gets the Terrain Type description.
	 * @return Terrain description
	 */
	public String getDescription() { return description; }
	
	/**
	 * Gets the Terrain Type base movement cost.
	 * @return Base movement cost in abstract units
	 */
	public short getMovementCost() { return movementCost; }
	
	/**
	 * Gets the Terrain Type defence modifier.
	 * @return Defence modifier (from -100 to +100)
	 */
	public short getDefenceModifier() { return defenceModifier; }
	
	/**
	 * Indicates if the terrain can be traversed or not.
	 * @return True if the terrain is interdicted to all units, false otherwise
	 */
	public boolean getIsInterditcted() { return interdicted; }
	
	/**
	 * Gets the Terrain Type to which this terrain degrades over time.
	 * @return Degraded terrain type (or null if there is no degradation)
	 */
	public TerrainType getDegradesTo() { return degradesToTerrain; }
	/**
	 * Number of turns necessary for the terrain to degrade.
	 * @return Number of turns (will be 0 by default)
	 */
	public int getDegradesToTurns() { return degradesToTurns; }
	
	/**
	 * Gets the Terrain Type to which this terrain transforms over time.
	 * @return Transformed terrain type (or null if there is no transformation)
	 */
	public TerrainType getTransformsTo() { return transformsToTerrain; }
	/**
	 * Number of turns necessary for the terrain to transform.
	 * @return Number of turns (will be 0 by default)
	 */
	public int getTransformsToTurns() { return transformsToTurns; }
	
	public String toString() { return this.identifier; }
}
