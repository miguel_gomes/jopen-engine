package org.jopenengine.containers;

public interface TerrainTile {

	/**
	 * Gets the terrain type.
	 * @return Terrain type
	 */
	TerrainType getTerrainType();
	
}
