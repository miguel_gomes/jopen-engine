/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.managers;

import java.util.HashMap;

import org.jopenengine.containers.TerrainMap;
import org.jopenengine.exceptions.managers.NoSuchMapException;

/**
 * 
 * <h1>JOpen-Engine</h1>
 * <p><b>TerrainMapManager.java</b></p>
 * <p></p>
 * 
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 31 de Mar de 2011 23:41:45
 *
 */
public class TerrainMapManager {
	/** Terrain Map Manager */
	private static TerrainMapManager instance;
	/** Maps */
	private HashMap<String,TerrainMap> maps;
	
	/**
	 * Creates a new Terrain Map Manager instance.
	 */
	private TerrainMapManager() {
		maps = new HashMap<String,TerrainMap>();
	}
	
	/**
	 * Gets the instance of the Terrain Map Manager.
	 * @return The Terrain Map Manager instance
	 */
	public static TerrainMapManager getInstance() {
		if (instance == null)
			instance = new TerrainMapManager();
		return instance;
	}
	
	/**
	 * Gets the map associated with a given key.
	 * @param key Key for a given terrain map
	 * @return The associated terrain map or null if there is no such map
	 */
	public TerrainMap getTerrainMap(String key) {
		return maps.get(key);
	}
	
	/**
	 * Adds a new terrain map with a given key.
	 * <p>Note: if the key already exists the old map will be returned.</p>
	 * @param key Map key
	 * @param map Terrain map
	 * @return Replaced map if it existed or null if it is a new insertion
	 */
	public TerrainMap addTerrainMap(String key, TerrainMap map) {
		TerrainMap oldMap = maps.get(key);
		maps.put(key, map);
		return oldMap;
	}
	
	/**
	 * Removes the terrain map associated with a given key.
	 * @param key The key of the terrain map to remove
	 * @return The removed map or null if there was no map with the given key
	 */
	public TerrainMap removeTerrainMap(String key) {
		return maps.remove(key);
	}
	
	/**
	 * Loads a given terrain map from the local map store.
	 * @param mapName Name of the terrain map to load
	 * @return The loaded terrain map
	 * @throws NoSuchMapException When it wasn't possible to load the map
	 */
	public TerrainMap loadTerrain(String mapName) throws NoSuchMapException {
		return null;
	}
}
