/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.managers;

import java.util.HashMap;

import org.jopenengine.containers.TerrainTile;
import org.jopenengine.containers.TerrainType;
import org.jopenengine.containers.Tile;
import org.jopenengine.engines.EngineInterface;
import org.jopenengine.exceptions.managers.NoSuchTileSetException;
import org.jopenengine.renders.ColorCode;
import org.jopenengine.renders.RenderFrame;
import org.jopenengine.renders.RenderInterface;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Tile Manager</b></p>
 * <p>Manages tile definitions.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 31 de Mar de 2011 23:41:06
 *
 */
public class TileManager {
	/**
	 * <h1>JOpen-Engine</h1>
	 * <p><b>Tile Formats</b></p>
	 * <p>This enum lists the supported tile formats.</p>
	 *
	 * @author Miguel Gomes (alka.setzer@gmail.com)
	 *
	 * @since 1.0
	 * @version 31 de Mar de 2011 23:52:06
	 *
	 */
	public static enum TileFormat {
		/** Unknown tile format */
		UNKNOWN,
		/** Square tile format */
		SQUARE,
		/** Isometric tile format */
		ISOMETRIC,
		/** Hexagonal tile format */
		HEXAGONAL
	}
	
	/** Tile Manager */
	private static TileManager instance;
	/** These are the base tiles, new copies will be created from this base store */
	private HashMap<String,Tile> tileStore;
	/** Current tile format */
	private TileFormat tileFormat;
	/** Tile dimensions */
	private int[] tileDimensions;
	/** Registered render */
	private RenderInterface renderer;
	/** Registered engine */
	private EngineInterface engine;
	
	/**
	 * Creates a new Tile Manager instance.
	 */
	private TileManager() {
		tileStore = new HashMap<String,Tile>();
		tileFormat = TileFormat.UNKNOWN;
		tileDimensions = new int[]{0,0};
	}
	
	/**
	 * Gets the instance of the Tile Manager.
	 * @return The Tile Manager instance
	 */
	public static TileManager getInstance() {
		if (instance == null)
			instance = new TileManager();
		return instance;
	}
	
	/**
	 * Registers a new Render Interface object.
	 * @param render The new Render Interface object
	 */
	public void registerRenderer(RenderInterface render) {
		this.renderer = render;
	}
	
	/**
	 * Unregisters the current Render Interface object.
	 * @return The old render interface object or null if there was no registered render
	 */
	public RenderInterface unregisterRenderer() {
		RenderInterface oldRenderer = renderer;
		renderer = null;
		return oldRenderer;
	}
	
	/**
	 * Registers a new Engine Interface object.
	 * @param render The new Engine Interface object
	 */
	public void registerEngine(EngineInterface engine) {
		this.engine = engine;
	}
	
	/**
	 * Unregisters the current Engine Interface object.
	 * @return The old engine interface object or null if there was no registered render
	 */
	public EngineInterface unregisterEngine() {
		EngineInterface oldEngine = engine;
		engine = null;
		return oldEngine;
	}
	
	/**
	 * Loads a given tile set from the local tile store.
	 * @param tileSetIdentifier Name of the tile set to load
	 * @throws NoSuchTileSetException When it wasn't possible to load the tile set
	 */
	public void loadTileSetDefinitions(String tileSetIdentifier) throws NoSuchTileSetException {
		return;
	}
	
	/**
	 * Sets the tile dimensions.
	 * <p>Note: This is set automatically whenever tiles are loaded by the <code>loadTerrain(String)</code> method.</p>
	 * @param width Tile width
	 * @param height Tile height
	 * @see #loadTerrain(String)
	 */
	public void setTileDimensions(int width, int height) {
		tileDimensions[0] = width;
		tileDimensions[1] = height;
	}
	
	/**
	 * Sets the tile format.
	 * <p>Note: This is set automatically whenever tiles are loaded by the <code>loadTerrain(String)</code> method.</p>
	 * @param format Tile format
	 * @see #loadTerrain(String) 
	 */
	public void setTileFormat(TileFormat format) {
		tileFormat = format;
	}
	
	/**
	 * Gets the tile dimensions. (packed as an int array)
	 * @return An int array with the dimensions [width;height]
	 */
	public int[] getTileDimensions() { return tileDimensions; }
	
	/**
	 * Gets the tile format.
	 * @return Tile format
	 */
	public TileFormat getTileFormat() { return tileFormat; }
	
	/**
	 * Renders a tile.
	 * @param tile Tile to render
	 * @return Rendered tile
	 */
	public RenderFrame renderTile(TerrainTile tile) {
		RenderFrame frame = renderer.getEmptyFrame(tileDimensions[0], tileDimensions[1], false);
		// TODO change this to generic code
		switch (tile.getTerrainType()) {
			case LAKE:
			case OCEAN:
				frame.setColor(renderer.getColor(ColorCode.BLUE));
				frame.fillRect(0, 0, tileDimensions[0], tileDimensions[1]);
				break;
			case DESERT:
				frame.setColor(renderer.getColor(ColorCode.YELLOW));
				frame.fillRect(0, 0, tileDimensions[0], tileDimensions[1]);
				break;
			case GRASSLAND:
			case PLAINS:
				frame.setColor(renderer.getColor(ColorCode.GREEN));
				frame.fillRect(0, 0, tileDimensions[0], tileDimensions[1]);
				break;
			case GLACIER:
				frame.setColor(renderer.getColor(ColorCode.WHITE));
				frame.fillRect(0, 0, tileDimensions[0], tileDimensions[1]);
				break;
			case THUNDRA:
				frame.setColor(renderer.getColor(ColorCode.DARK_GRAY));
				frame.fillRect(0, 0, tileDimensions[0], tileDimensions[1]);
				break;
		}
		return frame;
	}
}
