/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.engines;

import org.jopenengine.containers.TerrainMap;
import org.jopenengine.exceptions.managers.NoSuchTileSetException;
import org.jopenengine.managers.TerrainMapManager;
import org.jopenengine.managers.TileManager;
import org.jopenengine.renders.RenderColor;
import org.jopenengine.renders.RenderInterface;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Generic Engine</b></p>
 * <p>This abstract class provides abstract implementations of common Engine methods.</p>
 * <p>Specific engines should extend where appropriate.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 30 de Mar de 2011 23:58:16
 *
 */
public abstract class GenericEngine implements EngineInterface {
	/** The Tile Manager */
	protected TileManager tileManager;
	/** The Terrain Map Manager */
	protected TerrainMapManager mapManager;
	/** Current Terrain Map */
	protected TerrainMap currentTerrain;
	/** Current border width (default: 1 px) */
	protected short borderWidth;
	/** Current border colour (default: LIGHT_GRAY) */
	protected RenderColor borderColor;
	/** Current background colour (default: BLACK) */
	protected RenderColor backgroundColor;
	/** Current registered render */
	protected RenderInterface render;
	/** Viewport settings */
	protected ViewportSettings viewport;
	/** 
	 * The absolute world dimensions, this is calculated by doing the following:<p><code>
	 * x = numberOfTerrainColumns * tileDimensionX + (numberOfTerrainColumns+1)*borderWidth<br />
	 * y = numberOfTerrainRows * tileDimensionY + (numberOfTerrainRows+1)*borderWidth</code></p>
	 */
	protected int[] worldDimensions;
	/** Indicates if tile set definitions have been loaded or not */
	protected boolean hasLoadedTileSetDefinitions;
	
	public GenericEngine() {
		borderWidth = 1;
		mapManager = TerrainMapManager.getInstance();
		tileManager = TileManager.getInstance();
		tileManager.registerEngine(this);
		viewport = new ViewportSettings();
		worldDimensions = new int[2];
		hasLoadedTileSetDefinitions = false;
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#setTerrain(org.jopenengine.containers.TerrainMap)
	 */
	@Override
	public void setTerrain(TerrainMap map) { 
		currentTerrain = map;
		if (hasLoadedTileSetDefinitions)
			calculateWorldDimensions();
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#loadTerrain(java.lang.String)
	 */
	@Override
	public void loadTerrain(String mapName) {
		currentTerrain = mapManager.getTerrainMap(mapName);
		if (hasLoadedTileSetDefinitions)
			calculateWorldDimensions();
	}
	
	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#loadTileSetDefinitions(java.lang.String)
	 */
	@Override
	public void loadTileSetDefinitions(String tileSetIdentifier) throws NoSuchTileSetException {
		tileManager.loadTileSetDefinitions(tileSetIdentifier);
		hasLoadedTileSetDefinitions = true;
		if (currentTerrain != null)
			calculateWorldDimensions();
	}
	
	/**
	 * Calculates the world absolute dimensions.
	 * <p>This is done using the following formula:<p><code>
	 * x = numberOfTerrainColumns * tileDimensionX + (numberOfTerrainColumns+1)*borderWidth<br />
	 * y = numberOfTerrainRows * tileDimensionY + (numberOfTerrainRows+1)*borderWidth</code></p>
	 * </p>
	 */
	protected void calculateWorldDimensions() {
		if (!hasLoadedTileSetDefinitions || currentTerrain == null) return;
		int x = currentTerrain.getColumnCount()*tileManager.getTileDimensions()[0]+(currentTerrain.getColumnCount()+1)*borderWidth;
		int y = currentTerrain.getRowCount()*tileManager.getTileDimensions()[1]+(currentTerrain.getRowCount()+1)*borderWidth;
		worldDimensions[0] = x;
		worldDimensions[1] = y;
		System.err.println("World dimensions = "+x+" x "+y);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#getCurrentTerrainMap()
	 */
	@Override
	public TerrainMap getCurrentTerrainMap() { return currentTerrain; }

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#setBorderWidth(int)
	 */
	@Override
	public void setBorderWidth(int borderWidth) { this.borderWidth = (short)borderWidth; }

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#setBorderColor(org.jopenengine.renders.RenderColor)
	 */
	@Override
	public void setBorderColor(RenderColor color) { borderColor = color; }

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#getBorderColor()
	 */
	@Override
	public RenderColor getBorderColor() { return borderColor; }

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#getBorderWidth()
	 */
	@Override
	public int getBorderWidth() { return borderWidth; }

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#setBackgroundColor(org.jopenengine.renders.RenderColor)
	 */
	@Override
	public void setBackgroundColor(RenderColor color) { backgroundColor = color; }

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#getBackgroundColor()
	 */
	@Override
	public RenderColor getBackgroundColor() { return backgroundColor; }

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#setViewportDefinitions(int, int, float)
	 */
	@Override
	public void setViewportDefinitions(int width, int height, float scale) {
		viewport.setDimensions(width, height);
		viewport.setScale(scale);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#setViewportDimensions(int, int)
	 */
	@Override
	public void setViewportDimensions(int width, int height) {
		viewport.setDimensions(width, height);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#registerRender(org.jopenengine.renders.RenderInterface)
	 */
	@Override
	public void registerRender(RenderInterface render) { 
		this.render = render;
		tileManager.registerRenderer(render);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#unregisterRender()
	 */
	@Override
	public RenderInterface unregisterRender() {
		RenderInterface oldRender = render;
		tileManager.unregisterRenderer();
		render = null;
		return oldRender;
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#setScale(float)
	 */
	@Override
	public void setScale(float scale) { viewport.setScale(scale); }

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#getScale()
	 */
	@Override
	public float getScale() { return viewport.getScale(); }
	
	/**
	 * Checks if the world fits inside the viewport window.
	 * @return True if the world fits, false otherwise
	 */
	protected boolean worldFitsInViewport() {
		if (worldDimensions == null && viewport == null) return false;
		float vs = viewport.getScale();
		return (worldDimensions[0] < viewport.getDimensions()[0]/vs && worldDimensions[1] < viewport.getDimensions()[1]/vs);
	}
}
