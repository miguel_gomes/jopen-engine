/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 *
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.engines;

import org.jopenengine.containers.TerrainMap;
import org.jopenengine.containers.Tile;
import org.jopenengine.exceptions.managers.NoSuchTileSetException;
import org.jopenengine.renders.RenderColor;
import org.jopenengine.renders.RenderFrame;
import org.jopenengine.renders.RenderInterface;
import org.jopenengine.managers.TerrainMapManager;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Engine Interface</b></p>
 * <p>The Engine Interface specifies the methods that each specific Engine must implement.</p>
 * <p>Engines are responsible for:
 * <ul><li>provide viewport (render) to absolute (terrain) coordinates</li> 
 * <li>executing actions (like unit movements, tile changes)</li>
 * <li>rendering the results of actions to a render frame</li></ul> 
 * <p>It is possible for engines to provide extra functionality but the methods referenced 
 * here should be the bare minimum implemented to allow cross-engine functionality.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * 
 * @since 1.0
 * @version 30 de Mar de 2011 23:49:32
 *
 */
public interface EngineInterface {
	/**
	 * Loads the tile set definitions to be used with the current map.
	 * @param tileSetIdentifier The tile identifier
	 * @throws NoSuchTileSetException When the given tile set could not be loaded
	 */
	public void loadTileSetDefinitions(String tileSetIdentifier) throws NoSuchTileSetException;
	
	/**
	 * Sets the terrain that should be used by the engine.
	 * @param map Terrain map to use
	 */
	public void setTerrain(TerrainMap map);
	
	/**
	 * Loads a map given its name.
	 * <p>Note: The map will be loaded by the Terrain Map Manager.</p>
	 * @param mapName Name of the map
	 * @see TerrainMapManager#loadTerrain(String)
	 */
	public void loadTerrain(String mapName);
	
	/**
	 * Gets the currently loaded terrain map.
	 * @return Current terrain map or null if no map is loaded
	 */
	public TerrainMap getCurrentTerrainMap();
	
	/**
	 * Sets the border width in pixels (this will also scale in the viewport)
	 * @param borderWidth Border width in pixels (set to 0 to disable)
	 */
	public void setBorderWidth(int borderWidth);
	
	/**
	 * Sets the colour of the border.
	 * @param color Border colour (set to null to use the default LIGHT_GRAY colour) 
	 */
	public void setBorderColor(RenderColor color);
	
	/**
	 * Gets the border colour.
	 * @return Border colour
	 */
	public RenderColor getBorderColor();
	
	/**
	 * Gets the unscaled border width in pixels.
	 * @return The unscaled border width
	 */
	public int getBorderWidth();
	
	/**
	 * Sets the background colour that should be present on the output frames.
	 * @param color Background colour (set to null to use the default BLACK colour)
	 */
	public void setBackgroundColor(RenderColor color);
	
	/**
	 * Gets the background colour of the output frames.
	 * @return Background colour
	 */
	public RenderColor getBackgroundColor();
	
	/**
	 * Gets the tile that is at the virtual position (viewport coordinates).
	 * @param x viewport position x
	 * @param y viewport position y
	 * @return Tile at given position
	 */
	public Tile getTileAtVirtualPosition(int x, int y);
	
	/**
	 * Gets the tile indext that is at the virtual position (viewport coordinates).
	 * @param x viewport position x
	 * @param y viewport position y
	 * @return Tile index at given position
	 */
	public int[] getTileIndexAtVirtualPosition(int x, int y);
	
	/**
	 * Gets the tile indext that is at the virtual position (viewport coordinates).
	 * @param x viewport position x
	 * @param y viewport position y
	 * @param offset offset from the tile top left of the tile at the given position (this is an output parameter)
	 * @return Tile index at given position
	 */
	public int[] getTileIndexAtVirtualPosition(int x, int y, int[] offset);
	
	/**
	 * Sets the view port definition settings.
	 * @param width viewport width
	 * @param height viewport height
	 * @param scale alias for zoom level, permitted range [0.5~2.0]  
	 */
	public void setViewportDefinitions(int width, int height, float scale);
	
	/**
	 * Sets the viewport dimensions.
	 * @param width viewport width
	 * @param height viewport height
	 * @see #setViewportDefinitions(int, int, float)
	 */
	public void setViewportDimensions(int width, int height);
	
	/**
	 * Registers a render interface.
	 * @param render Render interface
	 */
	public void registerRender(RenderInterface render);
	
	/**
	 * Unregisters the current render interface.
	 * @return Unregistered render interface or null there was no interface registered
	 */
	public RenderInterface unregisterRender();
	
	/**
	 * Moves the viewport window.
	 * @param howMuch How much to move the viewport, as in, the number of pixels from point A to point B (in viewport units)
	 * @param orientation Orientation of movement in degrees [0~360], anti-clockwise, 0 is in the usual place (1st Quadrant)
	 * @return True if the move can be executed, false otherwise
	 */
	public boolean moveViewport(int howMuch, int orientation);
	
	/**
	 * Selects a tile at given virtual position (viewport coordinates).
	 * @param x viewport position x
	 * @param y viewport position y
	 */
	public void selectTileAtVirtualPosition(int x, int y);
	
	/**
	 * Sets the scale level, as in the zoom level for the map.
	 * @param scale alias for zoom level, permitted range [0.5~2.0]  
	 */
	public void setScale(float scale);
	
	/**
	 * Gets the current scale level, as in the zoom level.
	 * @return Scale level (range: 0.5~2.0)
	 */
	public float getScale();
	
	/**
	 * Centres view on a given tile.
	 * @param row Tile row
	 * @param column Tile column
	 */
	public void centerOnTile(int row, int column);
	
	/**
	 * Gets the current rendered frame.
	 * <p>Please note that this will return the current prepared frame, 
	 * to which actions may or may not have been already applied. As such
	 * make sure you call the <code>update</code> method of the engine.</p>
	 * @param frame The RenderFrame where to write the output (if set to null a new RenderFrame will be created but supplying the frame is better)
	 * @return The current render frame.
	 * @see #update
	 */
	public RenderFrame getCurrentRenderFrame(RenderFrame frame);
	
	/**
	 * Updates the map after actions have taken place.
	 * <p>It is very important to call this method whenever you want to update the state
	 * of the engine, because no actions will be performed visually until you do so.</p>
	 */
	public void update();
}
