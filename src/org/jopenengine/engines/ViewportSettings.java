/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.engines;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Viewport Settings</b></p>
 * <p>Object that holds generic viewport settings.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 31 de Mar de 2011 22:23:01
 *
 */
public class ViewportSettings {
	/** Current viewport width */
	private int width;
	/** Current viewport height */
	private int height;
	/** Current viewport scale */
	private float scale;
	/** Current position of the top left pixel, X position, in relation with the engine map */ 
	private int x;
	/** Current position of the top left pixel, Y position, in relation with the engine map */ 
	private int y;
	
	/**
	 * Creates a new default viewport settings object.
	 */
	public ViewportSettings() {
		this(0,0,1,0,0);
	}
	
	/**
	 * Creates a new Viewport object to store viewport settings.
	 * @param width Viewport width
	 * @param height Viewport height
	 */
	public ViewportSettings(int width, int height) {
		this(width,height,1.0f,0,0);
	}
	
	/**
	 * Creates a new Viewport object to store viewport settings.
	 * @param width Viewport width
	 * @param height Viewport height
	 * @param scale Viewport scale (range: [0.5~2.0])
	 */
	public ViewportSettings(int width, int height, float scale) {
		this(width,height,scale,0,0);
	}
	
	/**
	 * Creates a new Viewport object to store viewport settings.
	 * @param width Viewport width
	 * @param height Viewport height
	 * @param scale Viewport scale (range: [0.5~2.0])
	 * @param x Absolute position of the viewport in regards to the engine map (X component)
	 * @param y Absolute position of the viewport in regards to the engine map (Y component)
	 */
	public ViewportSettings(int width, int height, float scale, int x, int y) {
		this.width = width;
		this.height = height;
		if (scale < 0.5f) scale = 0.5f;
		if (scale > 2.0f) scale = 2.0f;
		this.scale = scale;
		if (x < 0) x = 0;
		if (y < 0) y = 0;
		this.x = x;
		this.y = y;
	}

	/**
	 * Sets viewport dimensions.
	 * @param width Viewport width
	 * @param height Viewport height
	 */
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Sets viewport scale, an alias for zoom level.
	 * @param scale Viewport scale (range: [0.5~2.0])
	 */
	public void setScale(float scale) {
		if (scale < 0.5f) scale = 0.5f;
		if (scale > 2.0f) scale = 2.0f;
		this.scale = scale;
	}
	
	/**
	 * Sets the absolute position of the top pixel of the viewport with regards with the engine map.
	 * @param x Absolute position of the viewport in regards to the engine map (X component)
	 * @param y Absolute position of the viewport in regards to the engine map (Y component)
	 */
	public void setAbsolutePosition(int x, int y) {
		if (x < 0) x = 0;
		if (y < 0) y = 0;
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Gets the viewport dimensions. (packed as an int array)
	 * @return An int array with the dimensions [width;height]
	 */
	public int[] getDimensions() { return new int[]{width,height}; }
	
	/**
	 * Gets the viewport scale value, as in the current zoom level.
	 * @return The current scale value
	 */
	public float getScale() { return scale; }
	
	/**
	 * Gets the absolute position of the top pixel of the viewport with regards with the engine map. (packed as an int array)
	 * @return An int array with the position [x;y]
	 */
	public int[] getAbsolutePosition() { return new int[]{x,y}; }
}
