package org.jopenengine.engines.square;

import javax.swing.JFrame;

import org.jopenengine.containers.TerrainMap;
import org.jopenengine.containers.TerrainType;
import org.jopenengine.containers.Tile;
import org.jopenengine.exceptions.managers.NoSuchTileSetException;
import org.jopenengine.renders.ColorCode;
import org.jopenengine.renders.jvm6.Canvas2D;
import org.jopenengine.managers.TerrainMapManager;
import org.jopenengine.managers.TileManager;
import org.jopenengine.managers.TileManager.TileFormat;

public class TerrainTester extends JFrame{
	private static final long serialVersionUID = 1369928268689933403L;
	
	private Canvas2D canvas;
	private static SquareEngine engine;
	
	public TerrainTester() {
		super("Terrain Tester");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500,312);
		canvas = new Canvas2D();
		canvas.setEngine(engine);
		canvas.setSize(500, 312);
		engine.setBorderWidth(1);
		engine.setBorderColor(canvas.getColor(ColorCode.LIGHT_GRAY));
		engine.setBackgroundColor(canvas.getColor(ColorCode.BLACK));
		add(canvas);
		pack();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		engine = new SquareEngine();
		// first lets create a new terrain map (3 by 5)
		TerrainMap map = new TerrainMap(5,5);
		map.addTerrainTile(0, 0, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(0, 1, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(0, 2, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(0, 3, new Tile(TerrainType.PLAINS));
		map.addTerrainTile(0, 4, new Tile(TerrainType.PLAINS));
		map.addTerrainTile(1, 0, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(1, 1, new Tile(TerrainType.PLAINS));
		map.addTerrainTile(1, 2, new Tile(TerrainType.PLAINS));
		map.addTerrainTile(1, 3, new Tile(TerrainType.THUNDRA));
		map.addTerrainTile(1, 4, new Tile(TerrainType.GLACIER));
		map.addTerrainTile(2, 0, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(2, 1, new Tile(TerrainType.PLAINS));
		map.addTerrainTile(2, 2, new Tile(TerrainType.GRASSLAND));
		map.addTerrainTile(2, 3, new Tile(TerrainType.DESERT));
		map.addTerrainTile(2, 4, new Tile(TerrainType.GLACIER));
		map.addTerrainTile(3, 0, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(3, 1, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(3, 2, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(3, 3, new Tile(TerrainType.PLAINS));
		map.addTerrainTile(3, 4, new Tile(TerrainType.PLAINS));
		map.addTerrainTile(4, 0, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(4, 1, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(4, 2, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(4, 3, new Tile(TerrainType.OCEAN));
		map.addTerrainTile(4, 4, new Tile(TerrainType.OCEAN));
		TerrainMapManager mapManager = TerrainMapManager.getInstance();
		mapManager.addTerrainMap("world", map);
		try {
			engine.loadTileSetDefinitions("debug");
		} catch (NoSuchTileSetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TileManager tileManager = TileManager.getInstance();
		tileManager.setTileFormat(TileFormat.SQUARE);
		tileManager.setTileDimensions(64, 64);
		engine.setTerrain(map);
		engine.setBorderWidth(1);
		engine.setViewportDefinitions(500, 312, 2f);
		TerrainTester t = new TerrainTester();
		t.setVisible(true);
	}
}
