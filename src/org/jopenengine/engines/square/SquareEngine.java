/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.engines.square;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import org.jopenengine.containers.TerrainTile;
import org.jopenengine.containers.Tile;
import org.jopenengine.engines.EngineInterface;
import org.jopenengine.engines.GenericEngine;
import org.jopenengine.renders.ColorCode;
import org.jopenengine.renders.FrameTransform;
import org.jopenengine.renders.RenderColor;
import org.jopenengine.renders.RenderFrame;
import org.jopenengine.renders.RenderStroke;
import org.jopenengine.renders.jvm6.Jvm6Frame;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Square Engine</b></p>
 * <p>Square tile engine.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 31 de Mar de 2011 22:49:07
 *
 */
public class SquareEngine extends GenericEngine implements EngineInterface {
	// Border stroke
	private static RenderStroke borderStroke;
	private static RenderStroke defaultStroke;

	/**
	 * 
	 */
	public SquareEngine() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#setBorderWidth(int)
	 */
	@Override
	public void setBorderWidth(int borderWidth) { 
		super.setBorderWidth(borderWidth);
//		if (render != null) render.setStrokeWidth(borderWidth);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#getTileAtVirtualPosition(int, int)
	 */
	@Override
	public Tile getTileAtVirtualPosition(int x, int y) {
		int[] index = getTileIndexAtVirtualPosition(x,y);		
		return (Tile) currentTerrain.getTerrainTile(index[0], index[1]);
	}
	
	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#getTileIndexAtVirtualPosition(int, int)
	 */
	public int[] getTileIndexAtVirtualPosition(int x, int y) {
		return getTileIndexAtVirtualPosition(x,y,null);
	}
	
	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#getTileIndexAtVirtualPosition(int, int, int[])
	 */
	public int[] getTileIndexAtVirtualPosition(int x, int y, int[] offset) {
		int[] index = new int[2];
		int xOffset = 0;
		int yOffset = 0;
		int tx = tileManager.getTileDimensions()[0];
		int ty = tileManager.getTileDimensions()[1];
		float t0x, t0y, r0x, r0y;
		t0x = (float)x/(float)tx;
		t0y = (float)y/(float)ty;
		// the integer part of this is the exact number of tiles we need
		t0x += t0x*borderWidth / (float)tx; 
		t0y += t0y*borderWidth / (float)ty;
		index[0] = (int) t0x;
		index[1] = (int) t0y;
		// We will only calculate the offset if explicitly requested
		if (offset != null) {
			// the remainder is the offset from the tile top left pixel
			r0x = t0x % 1;
			r0y = t0y % 1;
			xOffset = (int) (r0x * tx);
			yOffset = (int) (r0y * ty);
			offset[0] = xOffset;
			offset[1] = yOffset;
		}
//		System.err.println("At virtual position ("+x+", "+y+") the tile index is ["+index[0]+", "+index[1]+"] and the offset is: "+xOffset+", "+yOffset);
		return index;
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#moveViewport(int, int)
	 */
	@Override
	public boolean moveViewport(int howMuch, int orientation) {
		System.err.println("Moving viewport "+howMuch+" units in the following orientation: "+orientation);
		int curPos[] = viewport.getAbsolutePosition();
		double radians = orientation * (Math.PI / 180.0);
		int x = (int) (curPos[0] + howMuch * Math.cos(radians));
		int y = (int) (curPos[1] + howMuch * Math.sin(radians));
		System.err.println("Viewport will change it's top left from ("+curPos[0]+","+curPos[1]+") to ("+x+","+y+")");
		// now check if the move can be executed or not
		if (x < 0 || y < 0 || x > worldDimensions[0] || y > worldDimensions[1]) {
			System.err.println("Move can not be executed");
			return false;
		}
		viewport.setAbsolutePosition(x, y);
		return true;
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#selectTileAtVirtualPosition(int, int)
	 */
	@Override
	public void selectTileAtVirtualPosition(int x, int y) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#centerOnTile(int, int)
	 */
	@Override
	public void centerOnTile(int row, int column) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#getCurrentRenderFrame(org.jopenengine.renders.RenderFrame)
	 */
	@Override
	public RenderFrame getCurrentRenderFrame(RenderFrame frame) {
		// TODO Auto-generated method stub
		int vx, vy, vx0, vy0, wx, wy;
		int tx = tileManager.getTileDimensions()[0];
		int ty = tileManager.getTileDimensions()[1];
		float vs;
		boolean worldFits = worldFitsInViewport();
		vx = viewport.getDimensions()[0];
		vy = viewport.getDimensions()[1];
		vs = viewport.getScale();
		vx0 = viewport.getAbsolutePosition()[0];
		vy0 = viewport.getAbsolutePosition()[1];
		wx = worldDimensions[0];
		wy = worldDimensions[1];
		int startX = worldFits ? (vx - wx) / 2 : 0;
		int startY = worldFits ? (vy - wy) / 2 : 0;
		int hMTx = (int)Math.ceil((vx/vs)/(double)tx);
		int hMTy = (int)Math.ceil((vy/vs)/(double)ty);
		int howManyTiles[] = new int[]{hMTx+1,hMTy+1};
		System.out.println("State: viewport ("+vx+", "+vy+", "+vs+"), world ["+wx+", "+wy+"], true viewport ["+(vx/vs)+", "+(vy/vs)+"], v0 ["+vx0+", "+vy0+"], world fits? "+worldFits+", will fit ["+howManyTiles[0]+", "+howManyTiles[1]+"]");
		// get tile at position
		int[] tileOffset = new int[2];
		int[] tileIndex = getTileIndexAtVirtualPosition(vx0, vy0, tileOffset);
		int bX = howManyTiles[0]*tx+(howManyTiles[0]+1)*borderWidth;
		int bY = (howManyTiles[1])*ty+(howManyTiles[1]+1)*borderWidth;
		System.out.println("Buffer: "+bX+", "+bY);
		RenderFrame buffer = render.getEmptyFrame(bX,bY,false);
		buffer.setColor(backgroundColor);
		buffer.fillRect(0, 0, buffer.getWidth(), buffer.getHeight());
		int lastY = 0;
		int lastX = 0;
		for (int y = tileIndex[1]; y < tileIndex[1]+howManyTiles[1]; y++) {
			lastX = 0;
			for (int x = tileIndex[0]; x < tileIndex[0]+howManyTiles[0]; x++) {
				TerrainTile tile = currentTerrain.getTerrainTile(y, x);
				if (tile == null) continue;
//				System.out.println("["+y+", "+x+"] "+tile.toString());
				if (borderWidth > 0) { // draw borders
					// first set the stroke
					buffer.setColor(borderColor);
//					buffer.setStroke(borderStroke);
/*
					System.out.println("\tHorizontal: ("+(x*tx)+", "+(y*ty)+") -> ("+(x*tx+tx+1)+", "+(y*ty)+")");
					System.out.println("\tVertical: ("+(x*tx)+", "+(y*ty)+") -> ("+(x*tx)+", "+(y*ty+ty+1)+")");
*/
					// horizontal line on the top of the tile
					buffer.drawLine(lastX, lastY, lastX+tx+borderWidth, lastY);
					// vertical line on the left of the tile
					buffer.drawLine(lastX, lastY, lastX, lastY+ty+borderWidth);
				}
//				buffer.setStroke(defaultStroke);
				// now ask for the tile render from the tile manager
				buffer.drawImage(tileManager.renderTile(tile), lastX+borderWidth, lastY+borderWidth);
				lastX += tx+borderWidth;
			}
			lastY += ty+borderWidth;
		}
		if (borderWidth > 0) {
//			buffer.setStroke(borderStroke);
			buffer.setColor(borderColor);
			// now draw the ending borders
			buffer.drawLine(0, lastY, buffer.getWidth(), lastY);
			buffer.drawLine(lastX, 0, lastX, buffer.getHeight());
		}
//		buffer.setStroke(defaultStroke);
		// check if we need to clip the image before putting it on frame
		if (!worldFits) {
			buffer.cropImage(tileOffset[0],tileOffset[1],(int)(vx/vs),(int)(vy/vs));			
		}
		// draw the buffer unto the frame
		if (vs == 1)
			frame.drawImage(buffer, 0, 0);
		else { // need to apply some scaling
			FrameTransform transform = render.getNewFrameTransform();
			transform.scale(vs, vs);
			frame.drawImage(buffer, transform, 0, 0);
		}
		return frame;
	}
	
	
	public int[] getTileOffsetAtVirtualPosition(int vx, int vy) {
		int x = 0;
		int y = 0;
		
		return new int[]{x,y};
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.engines.EngineInterface#update()
	 */
	@Override
	public void update() {
		// TODO Auto-generated method stub

	}
}
