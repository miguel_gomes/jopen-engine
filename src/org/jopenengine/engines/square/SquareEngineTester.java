package org.jopenengine.engines.square;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class SquareEngineTester extends JFrame {
	
	class Canvas2D extends Canvas implements MouseListener, KeyListener, MouseMotionListener {
		private static final long serialVersionUID = -7896329845765516329L;
		private int hoverX = -1;
		private int hoverY = -1;
		private int drawHeight;
		private int drawWidth;
		private int startX;
		private int startY;
		private int rows;
		private int columns;
		private int tileSize;
		private BufferedImage grid;
		private BufferedImage highlight;
		/** The double buffer will start loading data after this point */
		private final static float DBUFFER_LOAD = 1.25f;
		/** The maximum double buffer size (relative to the viewport size) */
		private final static float DBUFFER_MAX = 1.50f;
		
		public Canvas2D(int rows, int columns, int tileSize) {
			super();
			addMouseListener(this);
			addMouseMotionListener(this);
			addKeyListener(this);
			setBackground(Color.WHITE);
			this.rows = rows;
			this.columns = columns;
			this.tileSize = tileSize;
		}
		
		public void setSize(int width, int height) {
			super.setSize(width, height);
			System.out.println("resizing to "+width+" x "+height);
			drawHeight = tileSize*rows+(rows+1); // 1 pixel border
			drawWidth = tileSize*columns+(columns+1); // 1 pixel border
			startX = getWidth() / 2 - drawWidth / 2;
			startY = getHeight() / 2 - drawHeight / 2;
			System.out.println("StartX = "+startX+"\tdrawWidth = "+drawWidth+"\tcolumns = "+columns);
			System.out.println("startY = "+startY+"\tdrawHeight = "+drawHeight+"\trows = "+rows);
			grid = new BufferedImage(this.getWidth(),this.getHeight(),BufferedImage.TYPE_INT_RGB);
			/** draw the grid into the buffered image */
			// does it fit in the current dimensions?
			if (drawHeight < this.getHeight() && drawWidth < this.getWidth()) { // yei, no tricks! 
				// try to center the map in the center of the canvas
				Graphics2D g2d = (Graphics2D) grid.getGraphics();
				g2d.setBackground(Color.WHITE);
				g2d.clearRect(0, 0, grid.getWidth(), grid.getHeight());
				int curX = startX;
				int curY = startY;
				for (int y = 0; y < rows; y++) {
					curX = startX;
					for (int x = 0; x < columns; x++) { 
						g2d.setColor(Color.LIGHT_GRAY);
						g2d.drawRect(curX, curY, tileSize+1, tileSize+1);
						g2d.setColor(Color.WHITE);
						g2d.fillRect(curX+1, curY+1, tileSize, tileSize);
						curX += tileSize+1;
					}
					curY += tileSize+1;
				}
			}
			/** draw the highlight into the buffered image */
			highlight = new BufferedImage(tileSize+2,tileSize+2,BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = (Graphics2D) highlight.getGraphics();
			g2d.setBackground(Color.ORANGE);
			g2d.clearRect(0, 0, highlight.getWidth(), highlight.getHeight());
			g2d.setColor(Color.BLUE);
			g2d.drawRect(0, 0, tileSize+1, tileSize+1);
		}
		
		public void setMapSpecs(int rows, int columns, int tileSize) {
			this.rows = rows;
			this.columns = columns;
			this.tileSize = tileSize;
		}
		
		public void paint(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.drawImage(grid, null, 0, 0);
			if (hoverX > -1 && hoverY > -1)
				g2d.drawImage(highlight, null, (startX+(tileSize*hoverX)+hoverX), (startY+(tileSize*hoverY)+hoverY));
		}
		
		public void update(Graphics g) {
			paint(g);
		}

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			int x = e.getX();
			int y = e.getY();
			if (x < startX || x > startX+drawWidth) {
				hoverX = -1;
				hoverY = -1;
				return;
			}
			if (y < startY || y > startY+drawHeight) {
				hoverX = -1;
				hoverY = -1;
				return;
			}
			int newHoverX = (x - startX) / (tileSize + 2);
			int newHoverY = (y - startY) / (tileSize + 2);
			if (newHoverX != hoverX || newHoverY != hoverY) {
				hoverX = newHoverX;
				hoverY = newHoverY;
				repaint();
			}
		}
		
	}
	
	private static final long serialVersionUID = 6216576138321549697L;
	
	private int rows;
	private int columns;
	// tiles are squares
	private int tileSize;
	
	private Canvas2D canvas;
	
	public SquareEngineTester(String title, int rows, int columns, int tileSize) {
		setTitle(title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500,312);
		this.rows = rows;
		this.columns = columns;
		this.tileSize = tileSize;
		initCanvas();
	}
	
	public void setMapSpecs(int rows, int columns, int tileSize) {
		this.rows = rows;
		this.columns = columns;
		this.tileSize = tileSize;
		canvas.setMapSpecs(rows, columns, tileSize);
	}
	
	
	private void initCanvas() {
		canvas = new Canvas2D(rows, columns, tileSize);
		canvas.setSize(this.getWidth(),this.getHeight());
		canvas.setBackground(Color.WHITE);
		this.add(canvas);
		this.pack();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SquareEngineTester t = new SquareEngineTester("Square Tile Engine Tester",15,10,64); 
		// show it
		t.setVisible(true);
	}

}
