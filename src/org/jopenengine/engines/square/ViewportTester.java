package org.jopenengine.engines.square;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JFrame;

public class ViewportTester extends JFrame {
	
	class Point {
		public int x;
		public int y;
		public int z;
		
		public Point (int x, int y) { this(x,y,-1); }
		public Point (int x, int y, int z) { this.x = x; this.y = y; this.z = z; }
	}
	
	class Canvas2D extends Canvas implements MouseListener, KeyListener, MouseMotionListener {
		private static final long serialVersionUID = -7896329845765516329L;
		
		private int curTileX;
		private int curTileY;
		private int curHoverX;
		private int curHoverY;
		private int rows;
		private int columns;
		private float tileSizeX;
		private float tileSizeY;
		private float visibleWidth;
		private float visibleHeight;
		private int[] viewportCenter;
		
		
		
		private BufferedImage backBuffer;
		
		public Canvas2D (int numRows, int numColumns, int tileSize, int visibleWidth, int visibleHeight, int startTileX, int startTileY) {
			setSize(visibleWidth,visibleHeight);
			addKeyListener(this);
			addMouseListener(this);
			addMouseMotionListener(this);
			this.rows = numRows;
			this.columns = numColumns;
			this.curTileX = startTileX;
			this.curTileY = startTileY;
			this.curHoverX = curTileX;
			this.curHoverY = curTileY;
			/* calculate map dimensions */
			int mapX = (numColumns+2) * tileSize + numColumns+1;
			int mapY = (numRows+2) * tileSize + numRows+1;
			float scaleX = (float)visibleWidth / (float)mapX;
			float scaleY = (float)visibleHeight / (float)mapY;
			tileSizeX = tileSize * scaleX;
			tileSizeY = tileSize * scaleY;
			this.visibleWidth = visibleWidth * scaleX;
			this.visibleHeight = visibleHeight * scaleY;
			int centerX = ((int)curTileX+2) * (int)tileSizeX + curTileX + 1 - (int)(tileSizeX/2);
			int centerY = ((int)curTileY+2) * (int)tileSizeY + curTileY + 1 - (int)(tileSizeY/2);
			viewportCenter = new int[] { centerX, centerY };
			/* print some stats */
			System.out.println("The original map had a dimension of "+mapX+" x "+mapY+" pixels (this includes border and a padding tile to each side)");
			System.out.println("The virtual viewport is now "+((int)this.visibleWidth)+" x "+((int)this.visibleHeight)+" pixels");
			System.out.println("The virtual tile is now "+((int)this.tileSizeX)+" x "+((int)this.tileSizeY)+" pixels");
			backBuffer = new BufferedImage(visibleWidth,visibleHeight,BufferedImage.TYPE_INT_RGB);
			drawGrid((Graphics2D)backBuffer.getGraphics());
			paintCurrentTile((Graphics2D)backBuffer.getGraphics());
			drawViewports((Graphics2D)backBuffer.getGraphics());
		}
		
		public void paint(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.drawImage(backBuffer, null, 0, 0);
		}
		
		private void drawGrid(Graphics2D g2d) {
			g2d.setBackground(Color.WHITE);
			g2d.clearRect(0, 0, getWidth(), getHeight());
			/** draw the grid */
			int curX = 0;
			int curY = 0;
			for (int row = 0; row < rows+2; row++) {
				curX = 0;
				for (int column = 0; column < columns+2; column++) {
					if (row == 0 || row == rows+1 || column == 0 || column == columns+1) {
						// tile padding
						g2d.setColor(Color.BLACK);
						g2d.fillRect(curX+(column == 0 ? 0 : 1), curY+(row == 0 ? 0 : 1), (int)tileSizeX+(column == 0 ? 2 : 1), (int)tileSizeY+(row == 0 ? 2 : 1));
					} else { 
						// actual grid
						g2d.setColor(Color.LIGHT_GRAY);
						g2d.drawRect(curX, curY, (int)tileSizeX+1, (int)tileSizeY+1);
					}
					curX += tileSizeX+1;
				}
				curY += tileSizeY+1;
			}
		}
		
		private void paintCurrentTile(Graphics2D g2d) {
			paintTile(g2d,new Point(curTileX,curTileY),Color.GREEN);
		}
		
		private void paintTile(Graphics g, Point tile, Color color) {
			Graphics2D g2d = (Graphics2D)g;
			g2d.setColor(color);
			int x = ((int)tile.x+1) * (int)tileSizeX + tile.x + 2;
			int y = ((int)tile.y+1) * (int)tileSizeY + tile.y + 2;
			g2d.fillRect(x, y, (int)tileSizeX, (int)tileSizeY);
		}
		
		public ArrayList<Point> generateDiamondPoints(int x0, int y0, int radius, boolean includeCenter) {
			ArrayList<Point> points = new ArrayList<Point>();
			for (int i = 0; i <= radius; i++) {
				for (int k = 0; k <= radius; k++) {
					if (i == 0 && k == 0 && ! includeCenter) continue;
					if (i+k > radius) continue;
					points.add(new Point(x0 + i, y0 + k));
					points.add(new Point(x0 - i, y0 + k));
					points.add(new Point(x0 + i, y0 - k));
					points.add(new Point(x0 - i, y0 - k));
				}
			}
			return points;
		}
		
		public ArrayList<Point> generateCirclePoints(int x0, int y0, int radius, boolean includeCenter)
		{
			ArrayList<Point> points = new ArrayList<Point>();
			for (int rad = (includeCenter ? 0 : 1); rad <= radius; rad++) {
				int f = 1 - rad;
				int ddF_x = 1;
				int ddF_y = -2 * rad;
				int x = 0;
				int y = rad;
	
				points.add(new Point(x0,y0+rad));
				points.add(new Point(x0,y0-rad));
				points.add(new Point(x0 + rad,y0));
				points.add(new Point(x0 - rad,y0));
	
				while(x < y)
				{
					// ddF_x == 2 * x + 1;
					// ddF_y == -2 * y;
					// f == x*x + y*y - radius*radius + 2*x - y + 1;
					if(f >= 0) {
						y--;
						ddF_y += 2;
						f += ddF_y;
					}
					x++;
					ddF_x += 2;
					f += ddF_x;
					points.add(new Point(x0 + x, y0 + y));
					points.add(new Point(x0 - x, y0 + y));
					points.add(new Point(x0 + x, y0 - y));
					points.add(new Point(x0 - x, y0 - y));
					points.add(new Point(x0 + y, y0 + x));
					points.add(new Point(x0 - y, y0 + x));
					points.add(new Point(x0 + y, y0 - x));
					points.add(new Point(x0 - y, y0 - x));
				}
			}
			return points;
		}
		
		private void drawMovementArea(Graphics2D g2d, int moves) {
			ArrayList<Point> points = generateDiamondPoints(curTileX,curTileY,moves,false);
			for (Point tile: points) {
				paintTile(backBuffer.getGraphics(),tile,Color.RED);
			}
		}
		
		private void paintHoveredTile(Graphics2D g2d) {
			int x = ((int)curHoverX+1) * (int)tileSizeX + curHoverX + 1;
			int y = ((int)curHoverY+1) * (int)tileSizeY + curHoverY + 1;
			g2d.setColor(Color.BLUE);
			g2d.drawRect(x, y, (int)tileSizeX+1, (int)tileSizeY+1);
		}
		
		private void drawViewports(Graphics2D g2d) {
			boolean tileInsideViewport = isTileInsideViewport(curTileX,curTileY);
//			boolean tileInsideLoadZone = isTileInsideLoadZone(curTileX,curTileY);
			g2d.setColor(Color.RED);
			// find the coordinates of the current tile
			int centerX = viewportCenter[0];
			int centerY = viewportCenter[1];
			
			if (!tileInsideViewport) {
				centerX = ((int)curTileX+2) * (int)tileSizeX + curTileX + 1 - (int)(tileSizeX/2);
				centerY = ((int)curTileY+2) * (int)tileSizeY + curTileY + 1 - (int)(tileSizeY/2);
				viewportCenter[0] = centerX;
				viewportCenter[1] = centerY;
			}
			g2d.drawLine(centerX, centerY, centerX, centerY);
			g2d.setColor(Color.GREEN);
			g2d.drawRect(centerX-(int)(visibleWidth/2), centerY-(int)(visibleHeight/2), (int)visibleWidth, (int)visibleHeight);
			g2d.setColor(Color.ORANGE);
			g2d.drawRect(centerX-(int)(visibleWidth*1.25/2), centerY-(int)(visibleHeight*1.25/2), (int)(visibleWidth*1.25), (int)(visibleHeight*1.25));
			g2d.setColor(Color.RED);
			g2d.drawRect(centerX-(int)(visibleWidth*1.50/2), centerY-(int)(visibleHeight*1.50/2), (int)(visibleWidth*1.50), (int)(visibleHeight*1.50));			
		}
		
		public boolean isPointInsideViewArea(int x, int y, int xMod, int yMod, float multiplier) {
			int halfSizeX = (int)((visibleWidth/2)*multiplier);
			int halfSizeY = (int)((visibleHeight/2)*multiplier);
			return ((x - xMod) > (viewportCenter[0]-halfSizeX) &&
					(x + xMod) < (viewportCenter[0]+halfSizeX) &&
					(y - yMod) > (viewportCenter[1]-halfSizeY) &&
					(y + yMod) < (viewportCenter[1]+halfSizeY));
		}
		
		public boolean isTileInsideViewArea(int tileX, int tileY, float multiplier) {
			int centerX = ((int)curTileX+2) * (int)tileSizeX + curTileX + 1 - (int)(tileSizeX/2);
			int centerY = ((int)curTileY+2) * (int)tileSizeY + curTileY + 1 - (int)(tileSizeY/2);
			return isPointInsideViewArea(centerX,centerY,(int)(tileSizeX/2),(int)(tileSizeY/2),multiplier);
		}
		
		public boolean isTileInsideViewport(int tileX, int tileY) {
			return isTileInsideViewArea(tileX,tileY,1);
		}
		
		public boolean isTileInsideLoadZone(int tileX, int tileY) {
			return isTileInsideViewArea(tileX,tileY,1.50f);
		}
		
		public int[] tileAtPoint(int x, int y) {
			int[] tile = new int[] { -1 , -1 };
			if (isPointInsideViewArea(x,y,0,0,1)) {
				// figure out what point this is
				tile[0] = (int)((float)x / tileSizeX) - 1;
				tile[1] = (int)((float)y / tileSizeY) - 1;
			}
			return tile;
		}
		
		public void update(Graphics g) {
			paint(g);
		}

		@Override
		public void mouseDragged(MouseEvent e) {}

		@Override
		public void mouseMoved(MouseEvent e) {
			int[] tile = tileAtPoint(e.getX(),e.getY());
			if (tile[0] < 0 || tile [1] < 0) return;
			if (tile[0] == curHoverX && tile[1] == curHoverY) return;
			curHoverX = tile[0];
			curHoverY = tile[1];
			Graphics2D g2d = (Graphics2D)backBuffer.getGraphics();
			drawGrid(g2d);
			paintCurrentTile(g2d);
			drawViewports(g2d);
			paintHoveredTile(g2d);
			repaint();
		}

		@Override
		public void keyTyped(KeyEvent e) {}

		@Override
		public void keyPressed(KeyEvent e) {
			// move the curTile;
			switch (e.getKeyCode()) {
				case KeyEvent.VK_LEFT:
					if (curTileX <= 0) return;
					curTileX--;
					break;
				case KeyEvent.VK_RIGHT:
					if (curTileX >= columns-1) return;
					curTileX++;
					break;
				case KeyEvent.VK_UP:
					if (curTileY <= 0) return;
					curTileY--;
					break;
				case KeyEvent.VK_DOWN:
					if (curTileY >= rows-1) return;
					curTileY++;
					break;
				default:
					return;
			}
			curHoverX = curTileX;
			curHoverY = curTileY;
			Graphics2D g2d = (Graphics2D)backBuffer.getGraphics();
			drawGrid(g2d);
			paintCurrentTile(g2d);
			drawViewports(g2d);
			repaint();
		}

		@Override
		public void keyReleased(KeyEvent e) {}

		@Override
		public void mouseClicked(MouseEvent e) {
			boolean drawMovement = false;
			if (e.getButton() == MouseEvent.BUTTON1) {
				int[] tile = tileAtPoint(e.getX(),e.getY());
				if (tile[0] < 0 || tile [1] < 0) return;
				if (tile[0] == curTileX && tile[1] == curTileY) return;
				curTileX = tile[0];
				curTileY = tile[1];
				curHoverX = curTileX;
				curHoverY = curTileY;
			} else if (e.getButton() == MouseEvent.BUTTON3) {
				int[] tile = tileAtPoint(e.getX(),e.getY());
				if (tile[0] < 0 || tile [1] < 0) return;
				if (tile[0] != curTileX || tile[1] != curTileY) return;
				drawMovement = true;
			}
			Graphics2D g2d = (Graphics2D)backBuffer.getGraphics();
			drawGrid(g2d);
			paintCurrentTile(g2d);
			drawViewports(g2d);
			if (drawMovement) 
				drawMovementArea(g2d,2);
			repaint();
		}

		@Override
		public void mousePressed(MouseEvent e) {}

		@Override
		public void mouseReleased(MouseEvent e) {}

		@Override
		public void mouseEntered(MouseEvent e) {}
		
		@Override
		public void mouseExited(MouseEvent e) {}
	}

	private Canvas2D canvas;
		
	public ViewportTester(int numRows, int numColumns, int tileSize, int visibleWidth, int visibleHeight, int startTileX, int startTileY) {
		super("ViewPort Tester");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500,312);
		canvas = new Canvas2D(numRows,numColumns,tileSize,visibleWidth,visibleHeight,startTileX,startTileY);
		canvas.setSize(500,312);
		canvas.setBackground(Color.WHITE);
		add(canvas);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ViewportTester t = new ViewportTester(10,15,64,500,312,6,4);
		t.setVisible(true);
	}

}
