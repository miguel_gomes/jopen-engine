/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.renders;

/**
 * 
 * <h1>JOpen-Engine</h1>
 * <p><b>RenderFrame.java</b></p>
 * <p></p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 12 de Abr de 2011 23:14:16
 *
 */
public interface RenderFrame {
	/**
	 * Gets the current width of the frame.
	 * @return Current width
	 */
	public int getWidth();
	
	/**
	 * Gets the current height of the frame.
	 * @return Current height
	 */
	public int getHeight();
	
	/**
	 * Sets the foreground colour to use for operations.
	 * @param color Foreground colour
	 */
	public void setColor(RenderColor color);
	/**
	 * Sets the background colour to use for operations.
	 * @param color Background colour
	 */
	public void setBackgroundColor(RenderColor color);
	
	/**
	 * Crops the current Render Frame to a new size.
	 * @param x the x coordinate of the rectangle to intersect the clip with
	 * @param y the y coordinate of the rectangle to intersect the clip with
	 * @param width the width of the rectangle to intersect the clip with
	 * @param height the height of the rectangle to intersect the clip with
	 */
	public void cropImage(int x, int y, int width, int height);
	
	/**
	 * Draws a line, using the current colour, between the points (x1, y1) and (x2, y2) in this graphics context's coordinate system.
	 * @param x1 Point A x coordinate
	 * @param y1 Point A y coordinate
	 * @param x2 Point B x coordinate
	 * @param y2 Point B y coordinate
	 */
	public void drawLine(int x1, int y1, int x2, int y2);
	
	/**
	 * Draws the outline of the specified rectangle.
	 * @param x the x coordinate of the rectangle to be drawn.
	 * @param y the y coordinate of the rectangle to be drawn.
	 * @param width the width of the rectangle to be drawn.
	 * @param height the height of the rectangle to be drawn.
	 * @see #fillRect(int, int, int, int)
	 * @see #clearRect(int, int, int, int)
	 */
	public void drawRect(int x, int y, int width, int height);
	
	/**
	 * Fills the specified rectangle. The left and right edges of the rectangle are at x and x + width - 1. 
	 * The top and bottom edges are at y and y + height - 1. 
	 * The resulting rectangle covers an area width pixels wide by height pixels tall. 
	 * The rectangle is filled using the graphics context's current colour.
	 * @param x the x coordinate of the rectangle to be filled.
	 * @param y the y coordinate of the rectangle to be filled.
	 * @param width the width of the rectangle to be filled.
	 * @param height the height of the rectangle to be filled.
	 * @see #clearRect(int, int, int, int)
	 * @see #drawRect(int, int, int, int)
	 */
	public void fillRect(int x, int y, int width, int height);
	
	/**
	 * Clears the specified rectangle by filling it with the background colour of the current drawing surface. 
	 * This operation does not use the current paint mode. 
	 * The top and bottom edges are at y and y + height. 
	 * The background colour of off-screen images may be system dependent. 
	 * Applications should use setColor followed by fillRect to ensure that an off-screen image is cleared to a specific colour.
	 * @param x the x coordinate of the rectangle to clear.
	 * @param y the y coordinate of the rectangle to clear.
	 * @param width the width of the rectangle to clear.
	 * @param height the height of the rectangle to clear.
	 * @see #fillRect(int, int, int, int)
	 * @see #drawRect(int, int, int, int)
	 */
	public void clearRect(int x, int y, int width, int height);
	
	/**
	 * Renders a frame unto an already existing frame.
	 * @param frame Frame to render
	 * @param x the x coordinate of the location in user space where the upper left corner of the image is rendered
	 * @param y the y coordinate of the location in user space where the upper left corner of the image is rendered
	 */
	public void drawImage(RenderFrame frame, int x, int y);
	
	/**
	 * Renders a frame unto an already existing frame.
	 * @param frame Frame to render
	 * @param transform The transformation to apply
	 * @param x the x coordinate of the location in user space where the upper left corner of the image is rendered
	 * @param y the y coordinate of the location in user space where the upper left corner of the image is rendered
	 */
	public void drawImage(RenderFrame frame, FrameTransform transform, int x, int y);
}
