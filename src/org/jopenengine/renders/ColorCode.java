/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.renders;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Colour Codes</b></p>
 * <p>Colour codes that the Renderers can use to resolve to RenderColor objects.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 12 de Abr de 2011 23:58:46
 *
 */
public enum ColorCode {
	WHITE,
	LIGHT_GRAY,
	GRAY,
	DARK_GRAY,
	BLACK,
	RED,
	PINK,
	ORANGE,
	YELLOW,
	GREEN,
	MAGENTA,
	CYAN,
	BLUE
}
