/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.renders;

import org.jopenengine.engines.EngineInterface;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Render Interface</b></p>
 * <p></p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 31 de Mar de 2011 22:41:09
 *
 */
public interface RenderInterface {
	/**
	 * Gets a RenderColor object resolved from a ColorCode.
	 * @param code Colour code
	 * @return RenderColor object
	 */
	public RenderColor getColor(ColorCode code);
	/**
	 * Registers the engine that provides the information the render needs.
	 * @param engine Used engine (square, isometric or hexagonal)
	 */
	public void registerEngine(EngineInterface engine);
	
	/**
	 * Unregisters the current engine.
	 * @return The unregistered engine or null if there was no registered engine.
	 */
	public EngineInterface unregisterEngine();
	
	/**
	 * Gets a new render frame.
	 * <p>Note: this method is usually called by the engine.</p>
	 * @return A new render frame this rendered can process
	 */
	public RenderFrame getEmptyFrame();
	
	/**
	 * Gets a new render frame.
	 * <p>Note: this method is usually called by the engine.</p>
	 * @param width Frame width
	 * @param height Frame height
	 * @param hasAlpha Indicates whether or not the frame needs alpha support
	 * @return A new render frame this rendered can process
	 */
	public RenderFrame getEmptyFrame(int width, int height, boolean hasAlpha);
	
	/**
	 * Gets the a new FrameTransform object of the type supported by the render.
	 * @return A new FrameTransform object
	 */
	public FrameTransform getNewFrameTransform();
}
