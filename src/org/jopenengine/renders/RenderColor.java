/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 *
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.renders;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Render Color</b></p>
 * <p>Interface that indicates the methods Color wrappers should implement.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 31 de Mar de 2011 00:28:50
 *
 */
public interface RenderColor {
	/**
	 * Sets the blue component in the range 0-255 of the default sRGB space.
	 * @param blue the blue component
	 * @see #setRGB(int, int, int)
	 * @see #setRGB(int, int, int, int)
	 */
	public void setBlue(int blue);
	/**
	 * Sets the red component in the range 0-255 of the default sRGB space.
	 * @param red the red component
	 * @see #setRGB(int, int, int)
	 * @see #setRGB(int, int, int, int)
	 */
	public void setRed(int red);
	/**
	 * Sets the green component in the range 0-255 of the default sRGB space.
	 * @param green the green component
	 * @see #setRGB(int, int, int)
	 * @see #setRGB(int, int, int, int)
	 */
	public void setGreen(int green);
	/**
	 * Sets the alpha component in the range 0-255 of the default sRGB space.
	 * @param alpha the alpha component
	 * @see #setRGB(int, int, int)
	 * @see #setRGB(int, int, int, int)
	 */
	public void setAlpha(int alpha);
	/**
	 * Sets the red, green and blue components in the range 0-255 of the default sRGB space.
	 * @param red the red component
	 * @param green the alpha component
	 * @param blue the blue component
	 */
	public void setRGB(int red, int green, int blue);
	/**
	 * Sets the red, green, blue and alpha components in the range 0-255 of the default sRGB space.
	 * @param red the red component
	 * @param green the alpha component
	 * @param blue the blue component
	 * @param alpha the alpha component
	 */
	public void setRGB(int red, int green, int blue, int alpha);
	
	/**
	 * Returns the blue component in the range 0-255 in the default sRGB space.
	 * @return the blue component.
	 * @see #getRGB()
	 */
	public int getBlue();
	/**
	 * Returns the green component in the range 0-255 in the default sRGB space.
	 * @return the green component.
	 * @see #getRGB()
	 */
	public int getGreen();
	/**
	 * Returns the red component in the range 0-255 in the default sRGB space.
	 * @return the red component.
	 * @see #getRGB()
	 */
	public int getRed();
	/**
	 * Returns the alpha component in the range 0-255.
	 * @return the alpha component.
	 */
	public int getAlpha();
	/**
	 * Returns the RGB value representing the color in the default sRGB ColorModel. (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
	 * @return the RGB value of the color in the default sRGB ColorModel.
	 * @see #getAlpha()
	 * @see #getRed()
	 * @see #getGreen()
	 * @see #getBlue()
	 */
	public int getRGB();
	
	/**
	 * Parse the colour string, and return the corresponding colour-int. If the string cannot be parsed, throws an IllegalArgumentException exception. 
	 * Supported formats are: #RRGGBB #AARRGGBB 'red', 'blue', 'green', 'black', 'white', 'gray', 'cyan', 'magenta', 'yellow', 'lightgray', 'darkgray'
	 * @param color Colour to parse
	 */
	public void parseColor(String color);
	
}
