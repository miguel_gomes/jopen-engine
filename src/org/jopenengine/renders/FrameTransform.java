/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.renders;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Frame Transform</b></p>
 * <p>This interface defines the methods a Transformation applied to Frames need to have.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 14 de Abr de 2011 00:50:30
 *
 */
public interface FrameTransform {
	
	/**
	 * Concatenates an FrameTransform Tx to this FrameTransform Cx in the most commonly useful way 
	 * to provide a new user space that is mapped to the former user space by Tx. 
	 * Cx is updated to perform the combined transformation. 
	 * Transforming a point p by the updated transform Cx' is equivalent to first transforming p by Tx 
	 * and then transforming the result by the original transform Cx like this: 
	 * 
	 * Cx'(p) = Cx(Tx(p)) In matrix notation, if this transform Cx is represented by the matrix [this] 
	 * and Tx is represented by the matrix [Tx] then this method does the following:
	 * <p><pre>                [this] = [this] x [Tx]</pre></p>
	 * @param tx the FrameTransform object to be concatenated with this FrameTransform object.
	 */
	public void concatenate(FrameTransform tx);

	/**
	 * Concatenates this transform with a scaling transformation. 
	 * This is equivalent to calling concatenate(S), 
	 * where S is an FrameTransform represented by the following matrix:
	 * <p><pre>          [   sx   0    0   ]
	 *          [   0    sy   0   ]
	 *          [   0    0    1   ]</pre></p>
     *    
	 * @param sx the factor by which coordinates are scaled along the X axis direction
	 * @param sy the factor by which coordinates are scaled along the Y axis direction
	 */
	public void scale(double sx, double sy);
}
