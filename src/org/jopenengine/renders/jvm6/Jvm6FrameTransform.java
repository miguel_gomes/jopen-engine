/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.renders.jvm6;

import java.awt.geom.AffineTransform;

import org.jopenengine.renders.FrameTransform;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>JVM 6 Transform</b></p>
 * <p>This class wraps the JVM 6 API AffineTransform class so that we can apply some transformations to the Frames</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 14 de Abr de 2011 00:51:37
 *
 */
public class Jvm6FrameTransform implements FrameTransform {
	private AffineTransform _transform;
	
	/**
	 * Creates a new default JVM 6 Transform to implement the Frame Transform interface. 
	 */
	public Jvm6FrameTransform() {
		_transform = new AffineTransform();
	}
	
	/**
	 * Gets the backing AffineTransform object.
	 * @return The AffineTransform object that backs this wrapper
	 */
	public AffineTransform getBackingTransform() {
		return _transform;
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.FrameTransform#concatenate(org.jopenengine.renders.FrameTransform)
	 */
	@Override
	public void concatenate(FrameTransform tx) {
		_transform.concatenate(((Jvm6FrameTransform)tx).getBackingTransform());
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.FrameTransform#scale(double, double)
	 */
	@Override
	public void scale(double sx, double sy) {
		_transform.scale(sx, sy);
	}
}
