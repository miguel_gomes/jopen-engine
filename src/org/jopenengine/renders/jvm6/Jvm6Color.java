/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.renders.jvm6;

import java.awt.Color;

import org.jopenengine.renders.RenderColor;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Jvm 6 Color</b></p>
 * <p>This class implements the methods specified by the RenderColor interface backed by the JVM 6 Color class.</p>
 * @see Color
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 12 de Abr de 2011 00:23:05
 *
 */
public class Jvm6Color implements RenderColor {
	private Color _color;
	
	/** White */
	public static Jvm6Color WHITE = new Jvm6Color(255,255,255,255);
	/** Light gray */
	public static Jvm6Color LIGHT_GRAY = new Jvm6Color(192,192,192,255);
	/** Gray */
	public static Jvm6Color GRAY = new Jvm6Color(128,128,128,255);
	/** Dark gray */
	public static Jvm6Color DARK_GRAY = new Jvm6Color(64,64,64,255);
	/** Black */
	public static Jvm6Color BLACK = new Jvm6Color(0,0,0,255);
	/** Red */
	public static Jvm6Color RED = new Jvm6Color(255,0,0,255);
	/** Pink */
	public static Jvm6Color PINK = new Jvm6Color(255,175,175,255);
	/** Orange */
	public static Jvm6Color ORANGE = new Jvm6Color(255,200,0,255);
	/** Yellow */
	public static Jvm6Color YELLOW = new Jvm6Color(255,255,0,255);
	/** Green */
	public static Jvm6Color GREEN = new Jvm6Color(0,255,0,255);
	/** MAGENTA */
	public static Jvm6Color MAGENTA = new Jvm6Color(255,0,255,255);
	/** Cyan */
	public static Jvm6Color CYAN = new Jvm6Color(0,255,255,255);
	/** Blue */
	public static Jvm6Color BLUE = new Jvm6Color(0,0,255,255);
	
	/**
	 * Creates a new Jvm6 Color.
	 * @param red Red component value
	 * @param green Green component value
	 * @param blue Blue component value
	 */
	public Jvm6Color(int red, int green, int blue) {
		_color = new Color(red,green,blue);
	}
	
	/**
	 * Creates a new Jvm6 Color.
	 * @param red Red component value
	 * @param green Green component value
	 * @param blue Blue component value
	 * @param alpha Alpha component value
	 */
	public Jvm6Color(int red, int green, int blue, int alpha) {
		_color = new Color(red,green,blue, alpha);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#setBlue(int)
	 */
	@Override
	public void setBlue(int blue) {
		_color = new Color(this.getRed(), this.getGreen(), blue, this.getAlpha());
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#setRed(int)
	 */
	@Override
	public void setRed(int red) {
		_color = new Color(red, this.getGreen(), this.getBlue(), this.getAlpha());
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#setGreen(int)
	 */
	@Override
	public void setGreen(int green) {
		_color = new Color(this.getRed(), green, this.getBlue(), this.getAlpha());
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#setAlpha(int)
	 */
	@Override
	public void setAlpha(int alpha) {
		_color = new Color(this.getRed(), this.getGreen(), this.getBlue(), alpha);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#setRGB(int, int, int)
	 */
	@Override
	public void setRGB(int red, int green, int blue) {
		_color = new Color(red,green,blue);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#setRGB(int, int, int, int)
	 */
	@Override
	public void setRGB(int red, int green, int blue, int alpha) {
		_color = new Color(red,green,blue,alpha);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#getBlue()
	 */
	@Override
	public int getBlue() {
		return _color.getBlue();
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#getGreen()
	 */
	@Override
	public int getGreen() {
		return _color.getGreen();
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#getRed()
	 */
	@Override
	public int getRed() {
		return _color.getRed();
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#getAlpha()
	 */
	@Override
	public int getAlpha() {
		return _color.getAlpha();
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#getRGB()
	 */
	@Override
	public int getRGB() {
		return _color.getRGB();
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderColor#parseColor(java.lang.String)
	 */
	@Override
	public void parseColor(String color) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Gets the backing Color object.
	 * @return The backing Color object
	 * @see java.awt.Color
	 */
	public Color getBackingObject() { return _color; }

}
