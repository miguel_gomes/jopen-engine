package org.jopenengine.renders.jvm6;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import org.jopenengine.engines.EngineInterface;
import org.jopenengine.renders.ColorCode;
import org.jopenengine.renders.FrameTransform;
import org.jopenengine.renders.RenderColor;
import org.jopenengine.renders.RenderFrame;
import org.jopenengine.renders.RenderInterface;

public class Canvas2D extends Canvas implements MouseListener, KeyListener, MouseMotionListener, MouseWheelListener, RenderInterface {
	private static final long serialVersionUID = -7896329845765516329L;
	/** Engine */
	private EngineInterface engine;
	/** Key press movement amount */
	private static final int KEY_MOVE_AMOUNT = 10; 
	
	public Canvas2D() {
		super();
		addMouseListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);
		addKeyListener(this);
	}
	
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		engine.update();
		Jvm6Frame frame = (Jvm6Frame) engine.getCurrentRenderFrame(getEmptyFrame());
		// TODO The renderer should draw the interface here somewhere
		g2d.drawImage(frame.getGraphics(), null, 0, 0);
	}
	
	public void update(Graphics g) {
		paint(g);
	}
	
	public void setSize(int width, int height) {
		super.setSize(width, height);
	}
	
	public void setSize(Dimension d) {
		this.setSize(d.width, d.height);
	}
	
	public void setEngine(EngineInterface engine) {
		engine.registerRender(this);
		this.engine = engine;
	}

	@Override
	public void mouseDragged(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {}

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {
		boolean movePossible = false;
		switch(e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				movePossible = engine.moveViewport(KEY_MOVE_AMOUNT, 180);
				break;
			case KeyEvent.VK_RIGHT:
				movePossible = engine.moveViewport(KEY_MOVE_AMOUNT, 0);
				break;
			case KeyEvent.VK_UP:
				movePossible = engine.moveViewport(KEY_MOVE_AMOUNT, 90);
				break;
			case KeyEvent.VK_DOWN:
				movePossible = engine.moveViewport(KEY_MOVE_AMOUNT, 270);
				break;
			default:
				return;
		}
		// force repaint if move could be executed
		if (movePossible)
			repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void mouseClicked(MouseEvent e) {
		engine.getTileAtVirtualPosition(e.getX(), e.getY());
	}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderInterface#registerEngine(org.jopenengine.engines.EngineInterface)
	 */
	@Override
	public void registerEngine(EngineInterface engine) {
		this.engine = engine;
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderInterface#unregisterEngine()
	 */
	@Override
	public EngineInterface unregisterEngine() {
		EngineInterface oldEngine = engine;
		engine = null;
		return oldEngine;
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderInterface#getEmptyFrame()
	 */
	@Override
	public RenderFrame getEmptyFrame() {
		return getEmptyFrame(this.getWidth(),this.getHeight(),true);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderInterface#getEmptyFrame(int, int, boolean)
	 */
	@Override
	public RenderFrame getEmptyFrame(int width, int height, boolean hasAlpha) {
		return new Jvm6Frame(width,height,hasAlpha);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderInterface#getColor(org.jopenengine.renders.ColorCode)
	 */
	@Override
	public RenderColor getColor(ColorCode code) {
		switch(code) {
			case WHITE: return Jvm6Color.WHITE;
			case LIGHT_GRAY: return Jvm6Color.LIGHT_GRAY;
			case GRAY: return Jvm6Color.GRAY;
			case DARK_GRAY: return Jvm6Color.DARK_GRAY;
			case BLACK: return Jvm6Color.BLACK;
			case RED: return Jvm6Color.RED;
			case PINK: return Jvm6Color.PINK;
			case ORANGE: return Jvm6Color.ORANGE;
			case YELLOW: return Jvm6Color.YELLOW;
			case GREEN: return Jvm6Color.GREEN;
			case MAGENTA: return Jvm6Color.MAGENTA;
			case CYAN: return Jvm6Color.CYAN;
			case BLUE: return Jvm6Color.BLUE;
			default:
				 return Jvm6Color.PINK;
		}
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderInterface#getNewFrameTransform()
	 */
	@Override
	public FrameTransform getNewFrameTransform() {
		return new Jvm6FrameTransform();
	}
}