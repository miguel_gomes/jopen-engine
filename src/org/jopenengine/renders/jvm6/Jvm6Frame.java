/*
 * This file is part of the JOpen-Engine project.
 * For more information please refer to the project page at
 *
 *     http://code.google.com/p/jopen-engine
 * 
 * Copyright 2011 The JOpen-Engine Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jopenengine.renders.jvm6;

import java.awt.Graphics2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import org.jopenengine.renders.FrameTransform;
import org.jopenengine.renders.RenderColor;
import org.jopenengine.renders.RenderFrame;

/**
 * <h1>JOpen-Engine</h1>
 * <p><b>Jvm6 Frame</b></p>
 * <p>This is the backing Frame object for the RenderFrame interface under the JVM6.</p>
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 *
 * @since 1.0
 * @version 2 de Abr de 2011 15:43:04
 *
 */
public class Jvm6Frame implements RenderFrame {
	/** Actual frame */
	private BufferedImage _frame;
	/** Backing Graphics2D object */
	private Graphics2D g;
	
	/**
	 * Empty constructor used for clone
	 */
	private Jvm6Frame() {}
	
	/**
	 * Creates a new JVM 6 RenderFrame backed by a BufferedImage.
	 * @param width Frame width
	 * @param height Frame height
	 * @param hasAlpha Indicates whether or not the frame will have an alpha channel
	 */
	public Jvm6Frame(int width, int height, boolean hasAlpha) {
		_frame = new BufferedImage(width, height, (hasAlpha ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB));
		g = _frame.createGraphics();
	}
	
	public Jvm6Frame clone() {
		Jvm6Frame clone = new Jvm6Frame();
		BufferedImage bf = new BufferedImage(this._frame.getWidth(),this._frame.getHeight(),this._frame.getType());
		bf.setData(this._frame.getData());
		clone._frame = bf;
		return clone;
	}
	
	/**
	 * Gets the backing frame.
	 * @return The backing frame, in this case a BufferedImage
	 */
	public BufferedImage getGraphics() { return _frame; }

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#setColor(org.jopenengine.renders.RenderColor)
	 */
	@Override
	public void setColor(RenderColor color) {
		g.setColor(((Jvm6Color)color).getBackingObject());
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#setBackgroundColor(org.jopenengine.renders.RenderColor)
	 */
	@Override
	public void setBackgroundColor(RenderColor color) {
		g.setBackground(((Jvm6Color)color).getBackingObject());
		
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#drawLine(int, int, int, int)
	 */
	@Override
	public void drawLine(int x1, int y1, int x2, int y2) {
		g.drawLine(x1, y1, x2, y2);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#drawRect(int, int, int, int)
	 */
	@Override
	public void drawRect(int x, int y, int width, int height) {
		g.drawRect(x, y, width, height);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#fillRect(int, int, int, int)
	 */
	@Override
	public void fillRect(int x, int y, int width, int height) {
		g.fillRect(x, y, width, height);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#clearRect(int, int, int, int)
	 */
	@Override
	public void clearRect(int x, int y, int width, int height) {
		g.clearRect(x, y, width, height);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#getWidth()
	 */
	@Override
	public int getWidth() {
		return _frame.getWidth();
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#getHeight()
	 */
	@Override
	public int getHeight() {
		return _frame.getHeight();
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#drawImage(org.jopenengine.renders.RenderFrame, int, int)
	 */
	@Override
	public void drawImage(RenderFrame frame, int x, int y) {
		drawImage(frame, null, x, y);
	}
	
	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#drawImage(org.jopenengine.renders.RenderFrame, org.jopenengine.renders.FrameTransform, int, int)
	 */
	@Override
	public void drawImage(RenderFrame frame, FrameTransform transform, int x, int y) {
		if (transform == null)
			g.drawImage(((Jvm6Frame)frame).getGraphics(), null, x, y);
		else
			g.drawImage(((Jvm6Frame)frame).getGraphics(), new AffineTransformOp(((Jvm6FrameTransform)transform).getBackingTransform(),AffineTransformOp.TYPE_BICUBIC), x, y);
	}

	/* (non-Javadoc)
	 * @see org.jopenengine.renders.RenderFrame#cropImage(int, int, int, int)
	 */
	@Override
	public void cropImage(int x, int y, int width, int height) {
		// we have to make sure the width and height don't overshoot the frame
		if (x + width > _frame.getWidth()) {
			System.err.println("Jvm6Frame.cropImage("+x+", "+y+", "+width+", "+height+") Frame ["+_frame.getWidth()+", "+_frame.getHeight()+"] - X OVERRUN!!");
			width = _frame.getWidth() - x;
		}
		if (y + height > _frame.getHeight()) {
			System.err.println("Jvm6Frame.cropImage("+x+", "+y+", "+width+", "+height+") Frame ["+_frame.getWidth()+", "+_frame.getHeight()+"] - Y OVERRUN!!");
			height = _frame.getHeight() - y;
		}
		_frame = _frame.getSubimage(x, y, width, height);
		g = _frame.createGraphics();
	}
}
